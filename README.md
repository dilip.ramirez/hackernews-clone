### Hacker News Clone

<ul>
    <li><a href="#introduction">Introduction</a></li>
    <li><a href="#features">Features</a></li>
    <li><a href="#installation">Installation</a></li>
</ul>

#### Introduction

<p>Hacker News clone built with:</p>

<p><strong>Frontend</strong> (<a href="https://gitlab.com/dilip.ramirez/hn_frontend_ui">Repo</a>)</p>
<ul>
    <li>React</li>
    <li>Axios</li>
    <li>Redux (React Redux, React Thunk)</li>
    <li>React Router</li>
    <li>Material UI</li>
    <li>Styled Components</li>
    <li>nginx (w/ Rest API proxy)</li>
</ul>

<p><strong>Backend</strong> (<a href="https://gitlab.com/dilip.ramirez/hn-posts-server">Repo</a>)</p>
<ul>
    <li>Express</li>
    <li>Axios</li>
    <li>Mongoose</li>
</ul>

<strong>Database</strong>
<ul>
    <li>MongoDB</li>
</ul>

<p>This project is also <strong>fully dockerized</strong> (see docker-compose.yml)</p>


#### Features
<ul>
    <li>Display Hacker News posts using Algolia HN REST API</li>
    <li>Stores data in MongoDB</li>
</ul>


#### Installation

<p>Clone this repo and browse  <code>hackernews-clone</code></p>
<code>
    git clone git@gitlab.com:dilip.ramirez/hackernews-clone.git
</code>
<br />
<code>
    cd hackernews-clone
</code>
<p>Fetch submodules</p>
<code>
    git submodule update --init --recursive
</code>

<p>Build images using <code>docker-compose</code></p>
<code>
    docker-compose build
</code>

<p>Start containers in detached mode</p>
<code>
    docker-compose up -d
</code>